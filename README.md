# kmMidiMiniPCB

## Overview

PCB Project for [kmMidiMini Controller Application](https://gitlab.com/c-mos/avr/kmMidiMini)

This repository documents the **kmMidiMin** printed circuit board (PCB). The board is designed to handle MIDI OUT functionality and other peripheral interfacing components. Below are the details of the various connections and components on both the front and back sides of the PCB.

It is an open-hardware project licensed under the [**Open Hardware**](https://www.oshwa.org)

## Features

- **MIDI Output**: The board provides MIDI output functionality via the `J2 MIDI_OUT` connector.
- **Multiple Button Inputs**: Connect up to 4 external buttons for user input or control (`BTN1`, `BTN2`, `BTN3`, `BTN4`).
- **LED Indicators**: Various status LEDs (`STAT`, `PWR`) are included for visual feedback.
- **External Connections**: Multiple connectors allow interfacing with external devices, including I2C (`J7`, `SCL`, `SDA`), UART (`TX`, `RX`), and general-purpose IO pins.
- **Voltage Regulation**: The board features onboard voltage regulation for ADC reference voltage.
- **Reset and Programming**: Headers for In-System Programming (`ISP`, `OneWire`, `Bootloader USART`) are provided for easy programming and debugging.
- **IR Receiver Support**: The board can be interfaced with an infrared (IR) receiver for additional functionality.
- **Extendability** with [kmMiniShield-MIDI I/O](https://gitlab.com/c-mos/pcb/kmMiniShieldMidiIOPCB) and [kmMiniShield-RS232](https://gitlab.com/c-mos/pcb/kmMiniShieldRS232PCB) shields

## Schematics
<img src="https://gitlab.com/c-mos/pcb/kmMidiMiniPCB/-/raw/main/images/kmMidiMiniSchematics_v1.0.png" alt="kmMidiMini Schematics v 1.0" width="800" > <br>

## Peripherals
<img src="https://gitlab.com/c-mos/pcb/kmMidiMiniPCB/-/raw/main/images/kmMidiMiniPeripherals_v1.0.png" alt="kmMidiMini Schematics v 1.0" width="800" > <br>

## Board Overview
### Top Side

<img src="https://gitlab.com/c-mos/pcb/kmMidiMiniPCB/-/raw/main/oshView/01_BoardTop_Transparent.png" alt="Logo" width="800" > <br>

### Key Components:
- **MIDI OUT** (`J2`): The primary MIDI output connector.
- **Button Inputs** (`BTN1`, `BTN2`): Headers for connecting physical buttons.
- **ICSP Header** (`J5`): Used for in-circuit programming of the microcontroller (`U2`).
- **LED Indicators**:
  - `STAT`: Status indicator.
  - `PWR`: Power indicator.
- **Voltage Regulation** (`FSLP`): Provides power to external peripherals such as sensors or potentiometers.
- **Resistor Networks** (`RN1`, `RN2`): Pull-up or pull-down resistors for stabilizing button inputs.
- **Voltage Regulator** (`U1`): Ensures stable power delivery to the board.
- **Capacitors** (`C1` to `C17`): These are scattered throughout the board for noise suppression and power stabilization.
  
### Other Notable Connectors:
- **J1**: Power and auxiliary connectors.
- **J9**, **J10**: Additional peripheral connectors for expansion.

### Bottom Side

<img src="https://gitlab.com/c-mos/pcb/kmMidiMiniPCB/-/raw/main/oshView/02_BoardBottom_Transparent.png" alt="Logo" width="800" >

### Key Features:
- **Solder to Connect**: Multiple jumpers are available to customize the GPIOs (`JP3`, `JP4`, `JP5`, `JP6`).
- **AREF Control**: Options for selecting the Analog Reference Voltage (`AREF Internal`, `AREF External`).
- **SCK and ISP Headers** (`JP7`, `JP2`): Headers for in-system programming and connecting other devices.
- **Additional Analog and Digital Pins** (`ADC`, `AREF`, `AGND`): Can be used to connect various sensors or other peripherals.

### Silk Labels:
- **JP1-JP7**: Selection jumpers for configuring different features.
- **AREF**: Choose between internal and external reference for analog-to-digital conversions.
- **GitHub Repository**: The PCB references a GitHub repository for the firmware and hardware design files, as well as a YouTube channel for tutorials.

### BOM
Item #|Designator|Qty|Manufacturer|Mfg Part #|Description / Value|Package/Footprint |Type|Instructions / Notes|tme.eu|
--|--|--|--|--|--|--|--|--|--|
1|U1|1|Texas Instruments|[CD4069UBM96](https://www.ti.com/lit/ds/symlink/cd4069ub.pdf)|CD4069|SOIC-14_W3.9mm|SMD|Only needed when DIN 5 or MCON port is to be used for MIDI Transmittion|[Buy](https://www.tme.eu/gb/details/cd4069ubm96/gates-inverters/texas-instruments/)|
2|U2|1|Microchip|[ATMEGA328PB-AU](https://www.tme.eu/Document/d2651e2f0c6fcc87ccd79fd0f976244f/ATMEGA328PB-DTE.pdf)|ATMEGA328PB-AU|TQFP-32_7x7mm|SMD|For hobby projects Atmega 8, 88, 16, 168 etc. can also be used as alternative|[Buy](https://www.tme.eu/gb/details/atmega328pb-au/8-bit-avr-family/microchip-technology/)|
3|U3|1|Vishay|[TSOP31236](https://www.tme.eu/Document/6322d8eceed75d39c11c688bf7bdef1c/TSOP31236.pdf)|TSOP31236|CAST-3Pin|THT||[Buy](https://www.tme.eu/gb/details/tsop31236/ir-receiver-modules/vishay/)|
4|VR1|1|Texas Instruments|[REF3033AIDBZT](https://www.ti.com/lit/ds/symlink/ref3040.pdf)|REF3033AIDBZT|SOT-23-3|SMD|Only needed when ADC functionality is to be used with external voltage reference (shortcut AREF with VCC for internal AREF)|[Buy](https://www.tme.eu/gb/details/ref3033aidbzt/reference-voltage-sources-circuits/texas-instruments/)|
5|D1, D2|2|Diodes Inc.|[BZT52C3V3S-7-F](https://www.tme.eu/Document/6c3feb8a261e585d6c708a0721bcfcb7/BZT52CxxS_ser.pdf)|3V3|SOD-323|SMD|For internal reference voltage – shortcut VCC & AREF pins and do not connect JP1|[Buy](https://www.tme.eu/gb/details/bzt52c3v3s-7-f/smd-zener-diodes/diodes-incorporated/)|
6|D3|1|Fagor|[FSS16TRTB](https://www.tme.eu/Document/811fa05d1c0c63c062b085f38c991d2e/fss1_ser.pdf)|CDBA360-HF|D SMA|SMD|For protection only, might not be populated, e.g. when only USB Power is going to be used|[Buy](https://www.tme.eu/gb/details/fss16trtb/smd-schottky-diodes/fagor/fss16-trtb/)|
7|D4, D5, D6, D7|4|BIVAR|[SM1206GC-IL](https://www.tme.eu/Document/b95ad288e274e8f25728222841435d4d/SM1206GC-IL.pdf)|LED|LED_1206_3216|SMD|Only needed when Power and Status LEDS are going to be used|[Buy](https://www.tme.eu/gb/details/sm1206gc-il/smd-colour-leds/bivar/)|
8|C1, C2|2|Murata|[GRM0335C1E220FA01D](https://www.tme.eu/Document/58bb7db4269ac3ad53d1aa3c0ade9bab/GRM.pdf)|22pF|0603_1608Metric|SMD||[Buy](https://www.tme.eu/gb/details/grm0335c1e220fa01d/mlcc-smd-capacitors/murata/)|
9|C4|1|Kyocera AVX|[TAJA226K010R](https://www.tme.eu/Document/e24aafab2fa4d2fc335623965655fb70/TAJ.pdf)|22uF|CP_EIA-3216-18_Kemet-A|SMD||[Buy](https://www.tme.eu/gb/details/taja226k010r/smd-tantalum-capacitors/kyocera-avx/taja226k010rnj/)|
10|C11|1|Vishay|[TCSVS1D475KBAR](https://www.tme.eu/Document/b7b423fee79a0baa3802818aefa8a73f/293d.pdf)|4.7uF|CP_EIA-3216-18_Kemet-A|SMD||[Buy](https://www.tme.eu/gb/details/293d475x0010a2te3/smd-tantalum-capacitors/vishay/)|
11|C3, C5, C6, C7, C8, C9, C10, C12, C13, C14, C15, C16, C17|13|Samsung|[CL10B104JB8NNNC](https://www.tme.eu/Document/4a42202b32dab16128fe107dd69598cc/samsung-chip-cap.pdf)|100nF|0603_1608Metric|SMD||[Buy](https://www.tme.eu/gb/details/cl10b104jb8nnnc/mlcc-smd-capacitors/samsung/)|
12|RN1|1|Royal Ohm|[DR1206-1K2-4/8](https://www.tme.eu/Document/c2306f5fcae1834ada9fd99e0bdeec98/ROYALOHM-xDx.pdf)|1.2K|R_Array_Convex_4x0603|SMD|Only needed when build in LEDs are populated|[Buy](https://www.tme.eu/gb/details/dr1206-1k2-4_8/resistor-networks/royal-ohm/4d03wgj0122t5e/)|
13|RN2|1|Royal Ohm|[DR1206-10K-4/8](https://www.tme.eu/Document/c2306f5fcae1834ada9fd99e0bdeec98/ROYALOHM-xDx.pdf)|10K|R_Array_Convex_4x0603|SMD|Only needed when embedded Buttons or IR are going to be used|[Buy](https://www.tme.eu/gb/details/dr1206-10k-4_8/resistor-networks/royal-ohm/4d03wgj0103t5e/)|
14|R1, R10, R11|3|Royal Ohm|[SMD0805-4.75K-1%](https://www.tme.eu/Document/c283990e907c122bb808207d1578ac7f/POWER_RATING-DTE.pdf)|4K7|Resistor_SMD:R_0805_2012Metric|SMD||[Buy](https://www.tme.eu/gb/details/smd0805-4.75k-1%25/smd-resistors/royal-ohm/0805s8f4751t5e/)|
15|R8|1|Royal Ohm|[SMD0805-4.75K-1%](https://www.tme.eu/Document/c283990e907c122bb808207d1578ac7f/POWER_RATING-DTE.pdf)|4K7|Resistor_SMD:R_0805_2012Metric|SMD|Populate R8 for standard location of FSLP, or R8' for inverted|[Buy](https://www.tme.eu/gb/details/smd0805-4.75k-1%25/smd-resistors/royal-ohm/0805s8f4751t5e/)|
16|R2, R3|2|Royal Ohm|[CQ0805-68R-1%](https://www.tme.eu/Document/fd8030aace7fa4ecbd326eab752e0f92/CQ%20datasheet.pdf)|68R|Resistor_SMD:R_0805_2012Metric|SMD||[Buy](https://www.tme.eu/gb/details/cq0805-68r-1%25/smd-resistors/royal-ohm/cq05s8f680jt5e/)|
17|R4|1|Royal Ohm|[CQ0805-10K-1%](https://www.tme.eu/Document/c283990e907c122bb808207d1578ac7f/POWER_RATING-DTE.pdf)|10K|Resistor_SMD:R_0805_2012Metric|SMD||[Buy](https://www.tme.eu/gb/details/smd0805-10k-1%25/smd-resistors/royal-ohm/0805s8f1002t5e/)|
18|R5, R6|2|Royal Ohm|[CQ0805-220R-1%](https://www.tme.eu/Document/fd8030aace7fa4ecbd326eab752e0f92/CQ%20datasheet.pdf)|220R|Resistor_SMD:R_0805_2012Metric|SMD|Only needed when DIN 5 or MCON port is to be used for MIDI Transmittion|[Buy](https://www.tme.eu/gb/details/cq0805-220r-1%25/smd-resistors/royal-ohm/cq05s8f2200t5e/)|
19|R7|1|Royal Ohm|[CQ0805-180R-1%](https://www.tme.eu/Document/fd8030aace7fa4ecbd326eab752e0f92/CQ%20datasheet.pdf)|180R|Resistor_SMD:R_0805_2012Metric|SMD|Only needed when IR receiver is to be used|[Buy](https://www.tme.eu/gb/details/cq0805-180r-1%25/smd-resistors/royal-ohm/cq05s8f1800t5e/)|
20|L1|1|Viking|[NL03JTC100](https://www.tme.eu/Document/a05ae3504cfc60ed89e3a57fb94acf5e/VIKING-NL.pdf)|10uH|Inductor_SMD:L_0603_1608Metric|SMD||[Buy](https://www.tme.eu/gb/details/nl03jtc100/smd-0603-inductors/viking/)|
21|Y1|1|YIC|[16.00M-SMDHC49S](https://www.tme.eu/Document/6527d4ac6cd20f858e4312ccc80ef7ce/YIC-49US_49SMT-quartz_crystal.pdf)|16MHz|SMD_THC_HC49-SD|SMD|For hobby projects can be unpolulated in case internal clock is going to be used|[Buy](https://www.tme.eu/gb/details/16.00m-smdhc49s/smd-quartz-crystals/yic/)|
22|FSLP|1|Pololu|[Pololu_FSLP_#2730](https://www.pololu.com/file/0J750/FSLP-Integration-Guide-13.pdf)|||||[Buy](https://www.pololu.com/product/2730)|
23|J1|1|ECE|[ESB35101000Z-N](https://www.tme.eu/Document/72efeb4881439f19308d8bf71b9a43aa/ESB351010XXX%20DRAWING%2020230510.pdf)|548190519|USB_Mini_B_Female_690-005-299-043|SMD|Only needed in case USB Power or V-USB functionality is going to be used (for prototyping connect power to VCC_GNDD pins)|[Buy](https://www.tme.eu/gb/details/esb35101000z-n/usb-ieee1394-connectors/ece/esb35101000z/)|
24|J2|1|Cliff|[FM6725](https://www.tme.eu/Document/2ccc4daa3fb7e932572434f48f207b07/DIN-FM672x.pdf)|MIDI_OUT|MIDI_DIN5|THT||[Buy](https://www.tme.eu/gb/details/fm6725/din-connectors/cliff/)|
25|J3|1|Amphenol Communications Solutions|[77311-101-04LF](https://www.tme.eu/Document/cf95c6ed476410c00a081c8ff424e335/77311-101-01LF.pdf)|MIDI-USART|PinHeader_1x04_P2.54mm_Vertical|THT|Only needed in case external MIDI connector or second USART is going to be used|[Buy](https://www.tme.eu/gb/details/77311-101-04lf/pin-headers/amphenol-communications-solutions/)|
26|J4|1|Amphenol Communications Solutions|[67996-204HLF](https://www.tme.eu/Document/2d4a219a7dd0b1e98a08518b8e9e24c3/67996-XXXXLF.pdf)|MCON|PinHeader_3x02_P2.54mm_Vertical|THT|Only needed when DIN5 is not connected, and MIDI OUT is to be used via cable connector (with power on 2 pins), cable can be also soldered directly to the PCB|[Buy](https://www.tme.eu/gb/details/67996-204hlf/pin-headers/amphenol-communications-solutions/)|
27|J5|1|Amphenol Communications Solutions|[67996-208HLF](https://www.tme.eu/Document/2d4a219a7dd0b1e98a08518b8e9e24c3/67996-XXXXLF.pdf)|VCC_GNDD|PinHeader_2x04_P2.54mm_Vertical|THT|Only needed when connecting power or taking power to/from PCB is needed|[Buy](https://www.tme.eu/gb/details/67996-208hlf/pin-headers/amphenol-communications-solutions/)|
28|J6|1|CONNFLY ELECTRONIC|[ZL320-2X5P](https://www.tme.eu/Document/a3f73054ce53c2beb96c95af7253a843/DS1031-06.pdf)|uISP|PinHeader_2x05_P1.27mm_Vertical|THT|Only needed when IDC 1.27 pin is going to be used (e.g. ATMEL ICE Programmer)|[Buy](https://www.tme.eu/gb/details/zl320-2x5p/pin-headers/connfly/ds1031-06-2-5p8bv41-3a/)|
29|J7|1|Amphenol Communications Solutions|[67996-208HLF](https://www.tme.eu/Document/2d4a219a7dd0b1e98a08518b8e9e24c3/67996-XXXXLF.pdf)|AREF_GNDA|PinHeader_2x04_P2.54mm_Vertical|THT|Only needed when AREF is going to be used for external devices (like potentiomenters)|[Buy](https://www.tme.eu/gb/details/67996-208hlf/pin-headers/amphenol-communications-solutions/)|
30|J8|1|Amphenol Communications Solutions|[ZL263-6SG](https://www.tme.eu/Document/0b9c0167ae055487886b51d5a659171c/DS1024.pdf)|AIRPORT|PinSocket_1x06_P2.54mm_Vertical|THT|Only needed when AIRPORT is going to be used (ISP Programmer, ATNEL WIFI, ATNEL WIFI ESP, Bluetooth Airport, etc.)|[Buy](https://www.tme.eu/gb/details/zl263-6sg/pin-headers/connfly/ds1024-1-6rf1/)|
31|J9|1|Amphenol Communications Solutions|[77311-101-04LF](https://www.tme.eu/Document/cf95c6ed476410c00a081c8ff424e335/77311-101-01LF.pdf)|TWI|PinHeader_1x04_P2.54mm_Vertical|THT|Only needed when external I2C/TWI devices will be used (can be also female connector for connecting directly LCD/OLED displays)|[Buy](https://www.tme.eu/gb/details/77311-101-04lf/pin-headers/amphenol-communications-solutions/)|
32|J10|1|Amphenol Communications Solutions|[77311-101-04LF](https://www.tme.eu/Document/cf95c6ed476410c00a081c8ff424e335/77311-101-01LF.pdf)|ADC|PinHeader_1x04_P2.54mm_Vertical|THT|Only needed when external ADC is going to be used|[Buy](https://www.tme.eu/gb/details/77311-101-04lf/pin-headers/amphenol-communications-solutions/)|
33|J11|1|Amphenol Communications Solutions|[77311-101-04LF](https://www.tme.eu/Document/cf95c6ed476410c00a081c8ff424e335/77311-101-01LF.pdf)|BTN|PinHeader_1x04_P2.54mm_Vertical|THT|Only needed when external buttons are going to be used|[Buy](https://www.tme.eu/gb/details/77311-101-04lf/pin-headers/amphenol-communications-solutions/)|
34|BTN1, BTN2|2|Canal Electronic|[DTSM-32N-B](https://www.tme.eu/Document/fc4b091434be7442f4b5d56cf1ece96e/dtsm32n.pdf)|SW_Push|SW_SPST_REED_CT05-XXXX-G1|THT|External buttons can be used via BUTTON pins, or no buttons in e.g. only IR is going to be used|[Buy](https://www.tme.eu/gb/details/dtsm32nb/microswitches-tact/canal-electronic/dtsm-32n-b/)|
35|JP1|0|PCB|||||Connect to use Voltage Reference unit as ADC Reference||
36|JP2|0|PCB|||||Connect when IR receiver is going to be used (BUTTON4 not available in such case)||
37|JP3|0|PCB|||||Connect ADC0 (PC0) to GPIO0 (PD2)||
38|JP4|0|PCB|||||Connect ADC1 (PC1) to GPIO1 (PD7)||
39|JP5|0|PCB|||||Connect ADC2 (PC2) to GPIO2 (PD6) must be connected when FSLP used||
40|JP6|0|PCB|||||Connect ADC3 (PC3) to GPIO3 (PD5) must be connected when FSLP used||
41|JP7|0|PCB|||||Connect to use AIRPORT as ISP programmer, or SPI interface||



## Usage

This board is designed primarily for MIDI-based hardware projects, with additional support for sensors, buttons, and external peripherals. The variety of pin headers and connectors provides ample options for customization, and the board is programmable via standard ICSP or UART headers.

### Powering the Board:
- The board can be powered via the **PWR** connector or through external 5V/3.3V supplies as per the system's requirements.

## Links and Resources

You can find the design files and detailed schematics on the official repository:

- [Git Repository](https://gitlab.com/c-mos/pcb/kmMidiMiniPCB)
- [YouTube Tutorials EN](https://www.youtube.com/@e-lectronics)
- [YouTube Tutorials PL](https://www.youtube.com/@km-elektronika)

---

## Author and License

This project is released under the [**Open Hardware**](https://www.oshwa.org) license. You are free to use, modify, and distribute this design under the terms of the license. Please give proper attribution when sharing or modifying the design.

**Author**: Krzysztof Moskwa

**e-mail**: chris[dot]moskva[at]gmail[dot]com

**General License**: GPL-3.0-or-later

GNU General Public License (GPL) version 3.0 or later. See [LICENSE.txt](https://www.gnu.org/licenses/gpl-3.0.txt)

 ![GPL3 Logo](https://www.gnu.org/graphics/gplv3-or-later-sm.png)
